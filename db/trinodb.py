import trino

class TrinoDB:
    def __init__(self, host, port, user) -> None:
        self._connection = self.connect(host, port, user)
        self._cursor = self._connection.cursor()
    
    @property
    def connection(self):
        return self._connection
    
    @property
    def cursor(self):
        return self._cursor
    
    def connect(self, host, port, user):
        try:
            connection = trino.dbapi.connect(
                host=host,
                port=port,
                user=user,
                catalog='hive',
                schema='default',
            )
        except Exception as error:
            print('Error: connection to Trino not established {}'.format(error))
        else:
            print('Connection to Trino established')
        return connection
    
    def show_version(self):
        self.cursor.execute('select version()')
        result = self.cursor.fetchone()
        print(f'Version {result[0]}')
    
    def execute(self, sql) :
        self.cursor.execute(sql)
    
    def fetchall(self):
        return self.cursor.fetchall()

    def fetchone(self):
        return self.cursor.fetchone()

    def query_fetchall(self, sql):
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def query_fetchone(self, sql):
        self.cursor.execute(sql)
        return self.cursor.fetchone()
        