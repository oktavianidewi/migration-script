from clickhouse_driver import connect

class ClickhouseDB:
    def __init__(self, host, port, user, password, database) -> None:
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.database = database
        self._connection =  self.connect()
        self._cursor = self._connection.cursor()

        # result = self._cursor.execute('SHOW tables')
        # print(f'result atas {result}')
    
    @property
    def connection(self):
        return self._connection
    
    @property
    def cursor(self):
        return self._cursor
    
    def connect(self):
        try:
            connection =  connect(
                host=self.host, 
                port=self.port, 
                user=self.user, 
                password=self.password, 
                database=self.database
            )
        except Exception as error:
            print('Error: connection to Clickhouse not established {}'.format(error))
        else:
            print('Connection to Clickhouse established')
        return connection

    def show_version(self):
        self.cursor.execute('SELECT version()')
        result = self.cursor.fetchall()
        print(f'Version {result}')

    def execute(self, sql) :
        self.cursor.execute(sql)
    
    def fetchall(self):
        return self.cursor.fetchall()

    def fetchOne(self):
        return self.cursor.fetchOne()

    def query_fetchone(self, sql):
        self.cursor.execute(sql)
        return self.cursor.fetchone()
    
    def query_fetchall(self, sql):
        self.cursor.execute(sql)
        return self.cursor.fetchall()