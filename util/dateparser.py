
import datetime

class Dateparser:
    def __init__(self, date) -> None:
        self.start_date = date


    @property
    def start_date(self):
        return self._start_date
    @start_date.setter
    def start_date(self, value):
        self.validate_date_format_set('_start_date', value, 'start_date')

    
    @property
    def end_date(self):
        return self._end_date
    @end_date.setter
    def end_date(self, value):
        self.validate_date_format_set('_end_date', value, 'end_date')

    def validate_date_format_set(self, property_name, value, field_title):
        if value is None :
            raise ValueError('{} cannot be empty.'.format(field_title))
        else:
            try:
                formatted = datetime.datetime.strptime(value, '%Y-%m-%d')
            except ValueError:
                raise ValueError('Incorrect {} format. It should be YYYY-MM-DD.'.format(field_title))
        setattr(self, property_name, formatted)