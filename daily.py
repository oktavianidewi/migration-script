# from db.clickhousedb import ClickhouseDB
from db.trinodb import TrinoDB
import datetime
import time
import sys

# class functionalities: 
# migration (dari trino ke CH)
# daily count validation (assert data per day_bucket antara trino dan CH)
# query comparison between ch and trino
class Migration:
    def __init__(self, db, sql_key, start_date, end_date) -> None:
        self.start_date = start_date
        self.end_date = end_date
        self.db = db
        self.sql_key = sql_key
    
    # TODO: next pindah dateparser
    @property
    def start_date(self):
        return self._start_date
    @start_date.setter
    def start_date(self, value):
        self.validate_date_format_set('_start_date', value, 'start_date')

    
    @property
    def end_date(self):
        return self._end_date
    @end_date.setter
    def end_date(self, value):
        self.validate_date_format_set('_end_date', value, 'end_date')

    def validate_date_format_set(self, property_name, value, field_title):
        if value is None :
            raise ValueError('{} cannot be empty.'.format(field_title))
        else:
            try:
                formatted = datetime.datetime.strptime(value, '%Y-%m-%d')
            except ValueError:
                raise ValueError('Incorrect {} format. It should be YYYY-MM-DD.'.format(field_title))
        setattr(self, property_name, formatted)
        
    def generate_day_bucket(self):
        generated_date = [ (self.start_date + datetime.timedelta(days=x)).strftime("%Y-%m-%d") for x in range( 0, (self.end_date - self.start_date).days+1 ) ]
        return generated_date
    
    # TODO: 
    # bisa jalankan query di platform db sesuai pilihan parameter: run('migration') atau run('count_rows')
    
    def run(self):
        for day_bucket in self.generate_day_bucket():
            try:
                sql = trinosql_collections[self.sql_key].format(datestamp=day_bucket)

                # if db_type.lower() == 'trino' :
                #     sql = trinosql_collections[sql_key].format(datestamp=day_bucket)
                # elif db_type.lower() == 'clickhouse' :
                #     sql = clickhousesql_collections[sql_key].format(datestamp=day_bucket)

                print(f'Execute query on {day_bucket} with {sql}')
                start_timer = time.time()
                run_sql = self.db.query_fetchone(sql)
                end_timer = time.time()

                print(f'Row counts {run_sql}. Elapsed time: { round(end_timer-start_timer, 3) } seconds.')
            except:
                raise Exception('Error on executing query')
    
    # to compare row-counts and elapsed time between trino and clickhouse data
    def compare(self, sql_keys): 
        result = dict()
        for sql_key in sql_keys: 
            daily_result = dict()
            for day_bucket in self.generate_day_bucket():
                try:
                    sql = trinosql_collections[sql_key].format(datestamp=day_bucket)
                    print(f'Execute query on {day_bucket} with {sql}')
                    start_timer = time.time()
                    run_sql = self.db.query(sql)
                    end_timer = time.time()
                    daily_result[day_bucket] = run_sql[0][1], str(round(end_timer-start_timer, 3)) + ' seconds'
                    print(f'Row counts {run_sql[0]}. Elapsed time: { round(end_timer-start_timer, 3) } seconds.')
                except:
                    raise Exception('Error on executing query')
                result[sql_key] = daily_result
        print("result ", result)
        return result
    
    def analyze(self): 
        pass

class ChManager: 
    pass


clickhousesql_collections = {
    'show-tables' : 'show tables;',
    'test-query' : 'select * from conversation_logs_temp where day_bucket IN \'{datestamp}\' limit 1;',

    'monthly-rowcount-ch-convlogs' : 'select month_bucket, count() from test_kata_atomic_on_cluster.conversation_logs_temp where month_bucket IN \'2021-01\' group by month_bucket;'


}
trinosql_collections = {
    'migrate-convlogs' : 'INSERT INTO clickhouse2.test_kata_atomic_on_cluster.prod_conversation_logs_temp SELECT  to_utf8(id) AS id, to_utf8(month_bucket) AS month_bucket, process_at, to_utf8(environment_id) AS environment_id, to_utf8(channel_id) AS channel_id, to_utf8(user_id) AS user_id, to_utf8(bot_id) AS bot_id, to_utf8(bot_revision) AS bot_revision, to_utf8(channel_type) AS channel_type, created_at, to_utf8(day_bucket) AS day_bucket, duration, to_utf8(flow) AS flow, to_utf8(intent) AS intent, to_utf8(logname) AS logname, to_utf8(message_type) AS message_type, to_utf8(messages) AS messages, to_utf8(others) AS others, to_utf8(prev_session) AS prev_session, received_at, to_utf8(responses) AS responses, sent_at, to_utf8(session) AS session, session_start, to_utf8(state) AS state, to_utf8(text) AS text, user_created_at, DATE(datestamp) as datestamp, FROM_UNIXTIME(process_at/1000) as timestamp FROM hive."default".prod_conversation_logs WHERE datestamp = \'{datestamp}\'',
    'daily-rowcount-ch-convlogs' : 'SELECT datestamp, count(*) FROM clickhouse2.test_kata_atomic_on_cluster.prod_conversation_logs_temp where datestamp IN ( DATE(\'{datestamp}\') ) GROUP BY 1',
    'daily-rowcount-trino-convlogs' : 'SELECT datestamp, count(*) FROM hive.default.prod_conversation_logs WHERE datestamp = (\'{datestamp}\') GROUP BY 1',
    'monthly-rowcount-trino-convlogs' : 'SELECT month_bucket, count(*) FROM hive.default.prod_conversation_logs WHERE month_bucket IN (\'2021-01\') GROUP BY month_bucket',   
}

#  month_bucket value: ('2021-01', '2021-02', '2021-03', '2021-4', '2021-5', '2021-6', '2021-7', '2021-8', '2021-09', '2021-10')

def main():
    [filename, sql_key, start_date, end_date] = sys.argv
    
    # to access ch db from local, use AWS vpn
    # host_cluster = 'a842c5d5066d34f4cb663cb5168f6b7f-301067397.ap-southeast-1.elb.amazonaws.com'
    # host_server = '172.20.20.48'
    # clickhouseDB = ClickhouseDB(host_server, 9000, 'clickhouse_operator', 'clickhouse_operator_password', 'test_kata_atomic_on_cluster')
    # clickhouseDB.show_version()


    # TODO refactor manager untuk trino dan ch
    # chManager = Migration(clickhouseDB, start_date, end_date)
    # chManager.run('clickhouse', 'show-tables')

    

    # to access trino from local, use prod-kata vpn
    trinoDb = TrinoDB('34.101.251.102', '8080', 'dewi')
    trinoDb.show_version()

    migration = Migration(trinoDb, sql_key, start_date, end_date)
    migration.run()
    # migration.run('trino', 'daily-rowcount-trino-convlogs')
    # migration.compare(['daily-rowcount-ch-convlogs', 'daily-rowcount-trino-convlogs'])




if __name__ == "__main__":
    main()
