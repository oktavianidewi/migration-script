# from db.clickhousedb import ClickhouseDB
from db.trinodb import TrinoDB
import datetime
import time
import sys

# class functionalities: 
# migration (dari trino ke CH)
# daily count validation (assert data per day_bucket antara trino dan CH)
# query comparison between ch and trino
class Monthly:
    def __init__(self, db, sql_key, month_bucket, limit) -> None:
        self.month_bucket = month_bucket
        self.limit = int(limit)
        self.db = db
        self.sql_key = sql_key
    
    # TODO: validate month_bucket limit
    
    def maxCount(self): 
        sql = trinosql_collections['rowcount-prod-flow-intent-state-hourly'].format(month_bucket=self.month_bucket)
        print("sql ", sql)
        [month, count] = self.db.query_fetchone(sql)
        return count

    def run(self):
        max = self.maxCount()
        offsetval = 0

        try:
            while offsetval <= max:
                sql = trinosql_collections[self.sql_key].format(month_bucket=self.month_bucket, offset=offsetval, limit=self.limit)
                print(f'Execute query on {self.month_bucket} with {sql}')
                start_timer = time.time()
                run_sql = self.db.query_fetchone(sql)
                end_timer = time.time()
                print(f'Row counts {run_sql}. Elapsed time: { round(end_timer-start_timer, 3) } seconds.')
                offsetval += self.limit
        except:
            raise Exception('Error on executing query')

trinosql_collections = {
    'rowcount-prod-flow-intent-state-hourly' : 'SELECT month_bucket, count(*) FROM hive.default.prod_flow_intent_state_hourly WHERE month_bucket IN ({month_bucket}) GROUP BY month_bucket',   
    'migrate-flow-intent-state-hourly' : 'INSERT INTO clickhouse2.test_kata_atomic_on_cluster.prod_flow_intent_state_hourly_temp SELECT to_utf8(environment_id) AS environment_id, to_utf8(channel_id) AS channel_id, hour_bucket, to_utf8(flow) AS flow, to_utf8(intent) AS intent, to_utf8(state) AS state, total, month_bucket FROM hive."default".prod_flow_intent_state_hourly WHERE month_bucket IN ( {month_bucket} ) OFFSET {offset} LIMIT {limit}',
    'migrate-text-hourly' : 'INSERT INTO clickhouse2.test_kata_atomic_on_cluster.prod_text_hourly_temp SELECT to_utf8(environment_id) AS environment_id, to_utf8(channel_id) AS channel_id, hour_bucket, to_utf8(text) AS text, total, month_bucket FROM hive."default".prod_text_hourly WHERE month_bucket IN ( {month_bucket} ) OFFSET {offset} LIMIT {limit}'
}

#  month_bucket value: ('2021-01', '2021-02', '2021-03', '2021-4', '2021-5', '2021-6', '2021-7', '2021-8', '2021-09', '2021-10')

def main():
    [filename, sql_key, month_bucket, limit] = sys.argv

    # to access trino from local, use prod-kata vpn
    trinoDb = TrinoDB('34.101.251.102', '8080', 'dewi')
    trinoDb.show_version()

    monthlyMigration = Monthly(trinoDb, sql_key, month_bucket, limit)
    monthlyMigration.run()
    # migration.run('trino', 'daily-rowcount-trino-convlogs')
    # migration.compare(['daily-rowcount-ch-convlogs', 'daily-rowcount-trino-convlogs'])

if __name__ == "__main__":
    main()
